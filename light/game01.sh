#!/bin/bash
# Проверяем, является ли ОС Linux на базе RedHat
if [ -f /etc/redhat-release ]; then
		if [ -f "/vagrant/.ssh/id_rsa-remotessh.pub" ]; then
    	cat /vagrant/.ssh/id_rsa-remotessh.pub >> /home/vagrant/.ssh/authorized_keys
			echo "File '/vagrant/.ssh/id_rsa-remotessh.pu' coped"
		else
			echo "File '/vagrant/.ssh/id_rsa-remotessh.pu' not found!"
		fi
		sudo su -
    dnf update -y
    dnf install nano nginx curl mc -y
#    dnf clean all
		if [ ! -f /etc/nginx/conf.d/game01.conf ]; then
			cp /vagrant/game01.conf /etc/nginx/conf.d/
			chown -R root:root /etc/nginx/conf.d/game01.conf
			chmod 644 /etc/nginx/conf.d/game01.conf
		fi
		setenforce 0
		sed -i 's/^SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
		systemctl disable firewalld.service
		systemctl stop firewalld.service
		#firewall-cmd --zone=public --add-port=80/tcp --permanent
		#firewall-cmd --zone=public --add-port=8080/tcp --permanent
		#firewall-cmd --reload
		if [ ! -d /vhosts ]; then
			mkdir /vhosts
			mkdir /vhosts/game01
			cp -r /vagrant/game01-html/. /vhosts/game01/
			mkdir /vhosts/game01/app
			cp -r /vagrant/2048-game/. /vhosts/game01/app/
			cd /vhosts/game01/app
			curl -sL https://rpm.nodesource.com/setup_16.x | sudo -E bash -
			#curl -fsSL https://rpm.nodesource.com/setup_18.x | bash -
			dnf install -y nodejs
			npm install --include=dev
			npm run build
			chown -R nginx:nginx /vhosts
			chmod -R 555 /vhosts
		fi
		cat << EOF > /etc/systemd/system/game01.service
[Unit]
[Unit]
Description=Game01 App
After=network.target

[Service]
Type=simple
ExecStart=/bin/bash -c 'PATH=/vhosts/game01/app:$PATH exec npm start'
WorkingDirectory=/vhosts/game01/app
User=nginx
Group=nginx
Restart=always
RestartSec=3
Environment=PORT=8080

[Install]
WantedBy=multi-user.target
EOF
		systemctl daemon-reload
		systemctl start game01
		systemctl status game01
		systemctl enable game01
		if [ ! -d /vhosts/game01/log ]; then
			mkdir /vhosts/game01/log
		fi
		cd /vhosts/game01/log && ln -s /var/log/nginx/access.log access.log
		cd /vhosts/game01/log && ln -s /var/log/nginx/error.log error.log
		chmod -R 555 /vhosts/game01/log
	  cd /
		systemctl enable nginx.service
		systemctl start nginx.service
		chown -R root:nginx /var/log/nginx
		chmod -R 770 /var/log/nginx
else
    echo "Скрипт предназначен для работы на ОС redhat-like"
    exit 0
fi
