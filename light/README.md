task-240126: работа выполнена под среду запуска:
win11 + vmware workstation 17.x + vagrant 2.4.x (+ cmd: vagrant plugin install vagrant-vmware-desktop) + vagrant-vmware-utility_1.x + git

---

Требуется настроить желаемые хар-ки и сетевую адресацию ВМ в файле: vm-vmware.json

Разворачивание VM, и всех сервисов полностью автоматизировано (пока скрипты bash).
результат работы, после старта VM :
морда nginx: http://127.0.0.1:8081
(+ раздача логов nginx http://127.0.0.1:8081/log)
сама app: http://127.0.0.1:8080

PS: (порт 80 со старта занят, не стал трогать дефолт конфиг nginx, чтобы при обновлении версии, не было трудностей, файл сервера вынесен в отдельный vhost: /etc/nginx/conf.d/game01.conf).

[vagrant@game01-202 ~]$ sudo systemctl status game01.service
● game01.service - Game01 App
Loaded: loaded (/etc/systemd/system/game01.service; enabled; vendor preset: disabled)
Active: active (running) since Tue 2024-03-12 15:08:03 UTC; 10min ago
Main PID: 6197 (npm start)
Tasks: 22 (limit: 11401)
Memory: 97.9M
CGroup: /system.slice/game01.service
├─6197 npm start
└─6257 webpack

Mar 12 15:08:06 game01-202 bash[6197]: modules by path ./node_modules/game-2048/ 53.3 KiB
Mar 12 15:08:06 game01-202 bash[6197]: ./node_modules/game-2048/style/main.css 2.15 KiB [built] [code generated]
Mar 12 15:08:06 game01-202 bash[6197]: + 2 modules
Mar 12 15:08:06 game01-202 bash[6197]: modules by path ./node_modules/css-loader/dist/runtime/\*.js 2.33 KiB
Mar 12 15:08:06 game01-202 bash[6197]: ./node_modules/css-loader/dist/runtime/noSourceMaps.js 64 bytes [built] [code generated]
Mar 12 15:08:06 game01-202 bash[6197]: ./node_modules/css-loader/dist/runtime/api.js 2.26 KiB [built] [code generated]
Mar 12 15:08:06 game01-202 bash[6197]: ./node_modules/ansi-html-community/index.js 4.16 KiB [built] [code generated]
Mar 12 15:08:06 game01-202 bash[6197]: ./node_modules/events/events.js 14.5 KiB [built] [code generated]
Mar 12 15:08:06 game01-202 bash[6197]: ./src/index.js 337 bytes [built] [code generated]
Mar 12 15:08:06 game01-202 bash[6197]: webpack 5.72.1 compiled successfully in 869 ms
[vagrant@game01-202 ~]$

[vagrant@game01-202 ~]$ sudo netstat -tunlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address Foreign Address State PID/Program name
tcp 0 0 0.0.0.0:22 0.0.0.0:_ LISTEN 1011/sshd
tcp 0 0 0.0.0.0:80 0.0.0.0:_ LISTEN 6256/nginx: master
tcp 0 0 0.0.0.0:8081 0.0.0.0:_ LISTEN 6256/nginx: master
tcp6 0 0 :::22 :::_ LISTEN 1011/sshd
tcp6 0 0 :::8080 :::_ LISTEN 6257/webpack
tcp6 0 0 :::80 :::_ LISTEN 6256/nginx: master
udp 0 0 127.0.0.1:323 0.0.0.0:_ 907/chronyd
udp6 0 0 ::1:323 :::_ 907/chronyd
[vagrant@game01-202 ~]$
